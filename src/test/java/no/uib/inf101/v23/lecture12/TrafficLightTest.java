package no.uib.inf101.v23.lecture12;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TrafficLightTest {

	@Test
	void canConstruct() {
		TrafficLight light = new TrafficLight();
		assertTrue(light.redIsOn());
		assertFalse(light.greenIsOn());
		assertFalse(light.yellowIsOn());
	}

	@Test
	void canConstructWithInput() {
		boolean[] red = {true,false,false};
		boolean[] yellowRed = {true,true,false};
		boolean[] green = {false,false,true};
		boolean[] yellow = {false,true,false};
		testLightState(red);
		testLightState(yellowRed);
		testLightState(green);
		testLightState(yellow);
	}

	private void testLightState(boolean[] lights) {
		testLightState(lights[0], lights[1], lights[2]);
	}
	private void testLightState(boolean red, boolean yellow, boolean green) {
		TrafficLight light = new TrafficLight(red,yellow,green);
		assertEquals(red,light.redIsOn());
		assertEquals(yellow,light.yellowIsOn());
		assertEquals(green,light.greenIsOn());
		
	}

	@Test
	void canNotConstructWithInvalidInput() {
		boolean[] redGreen = {true,false,true};
		boolean[] yellowGreen = {false,true,true};
		boolean[] all = {true,true,true};
		boolean[] none = {false,false,false};
		assertThrows(IllegalArgumentException.class, () -> testLightState(redGreen));
		assertThrows(IllegalArgumentException.class, () -> testLightState(yellowGreen));
		assertThrows(IllegalArgumentException.class, () -> testLightState(all));
		assertThrows(IllegalArgumentException.class, () -> testLightState(none));
	}

	@Test
	void canCycleThroughAllStates() {
		TrafficLight light = new TrafficLight(true,false,false);
		assertTrue(light.redIsOn());
		assertTrue(!light.yellowIsOn());
		assertTrue(!light.greenIsOn());
		light.next();
		assertTrue(light.redIsOn());
		assertTrue(light.yellowIsOn());
		assertTrue(!light.greenIsOn());
		light.next();
		assertTrue(!light.redIsOn());
		assertTrue(!light.yellowIsOn());
		assertTrue(light.greenIsOn());
		light.next();
		assertTrue(!light.redIsOn());
		assertTrue(light.yellowIsOn());
		assertTrue(!light.greenIsOn());
		light.next();
		assertTrue(light.redIsOn());
		assertTrue(!light.yellowIsOn());
		assertTrue(!light.greenIsOn());
	}
}
