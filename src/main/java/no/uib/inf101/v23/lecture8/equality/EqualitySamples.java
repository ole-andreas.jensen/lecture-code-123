package no.uib.inf101.v23.lecture8.equality;

// Eksempler hentet fra:
// https://inf101.ii.uib.no/notat/typer/

import java.util.Objects;

public class EqualitySamples {
  public static void main(String[] args) {
    compareInts();
//    compareIntegers1();
//    compareIntegers2();
//    compareWithStrings();
//    compareWithNull1();
//    compareWithNull2();
//    compareWithNull3();
  }

  private static void compareInts() {
    int x = 12344;
    int y = 12345;
    int z = x + 1;
    System.out.println(x == y); // false
    System.out.println(y == z); // true
  }

  private static void compareIntegers1() {
    Integer x = 12344;
    Integer y = 12345;
    Integer z = x + 1;

    System.out.println(x == y); // false
    System.out.println(y == z); // false (?!)
  }

  private static void compareIntegers2() {
    Integer x = 12344;
    Integer y = 12345;
    Integer z = x + 1;

    System.out.println(x.equals(y)); // false
    System.out.println(y.equals(z)); // true
  }

  private static void compareWithStrings() {
    String foo = "foo";
    String fo = "fo";
    String s = fo + "o";
    System.out.println(s == foo); // false (?!)
    System.out.println(s.equals(foo)); // true
  }

  private static void compareWithNull1() {
    String a = null;
    String b = "foo";
    System.out.println(a.equals(b)); // NullPointerException
  }

  private static void compareWithNull2() {
    String a = null;
    String b = "foo";
    // Sjekk om a og b er like
    boolean aEqualsB;
    if (a == null) {
      aEqualsB = (b == null);
    } else {
      aEqualsB = a.equals(b);
    }
    System.out.println(aEqualsB);
  }

  private static void compareWithNull3() {
    String a = null;
    String b = "foo";
    boolean aEqualsB = Objects.equals(a, b);
    System.out.println(aEqualsB); // false
  }
}
