package no.uib.inf101.v23.lecture8.pokemon;

public class Main {

  public static void main(String[] args) {


    
    IPokemon pokemon1 = new Pokemon("Bidoof");




    IPokemon pokemon2 = new Pokemon("Feebas");
    System.out.println(pokemon1);
    System.out.println(pokemon2);
    int rounds = 0;

    while (pokemon1.isAlive() && pokemon2.isAlive()) {
      pokemon1.attack(pokemon2);
      if (pokemon2.isAlive()) {
        pokemon2.attack(pokemon1);
      }
      rounds++;
    }
    System.out.println(rounds + " rounds were played");
  }
}
