package no.uib.inf101.v23.lecture9inheritance.cmd;

public interface Command {

  String run(String[] args);

  String getName();
}
