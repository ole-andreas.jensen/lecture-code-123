package no.uib.inf101.v23.lecture9inheritance.grid;

import java.util.List;

public interface DoubleCollection {

  List<Double> getDoubles();
}
