package no.uib.inf101.v23.lecture9inheritance.cmd;

public class CmdAdd implements Command {
  @Override
  public String run(String[] args) {
    int sum = 0;
    for (String arg : args) {
      sum += Integer.parseInt(arg);
    }
    return String.valueOf(sum);
  }

  @Override
  public String getName() {
    return "add";
  }
}
