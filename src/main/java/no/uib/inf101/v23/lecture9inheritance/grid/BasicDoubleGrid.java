package no.uib.inf101.v23.lecture9inheritance.grid;

public interface BasicDoubleGrid extends GridDimension, DoubleCollection {

  void set(int row, int col, Double value);
  Double get(int row, int col);

}
