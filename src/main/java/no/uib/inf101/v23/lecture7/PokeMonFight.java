package no.uib.inf101.v23.lecture7;

public class PokeMonFight {

	public static void main(String[] args) {
		Pokemon pokemon1 = new Pokemon("Bulbasaur",70,20);
		Pokemon pokemon2 = new Pokemon("Snorlax",130,10);

        IPokemon winner = fight(pokemon1,pokemon2);
        System.out.println("The winner is "+winner+"!");
        
        RockPokemon pokemon3 = new RockPokemon("Geodude", 50, 30);
        pokemon3.rock();
        
        fight(winner,pokemon3);
	}
	
	static IPokemon fight(IPokemon attacker, IPokemon defender){
        System.out.println(attacker.getName()+" attacks "+defender.getName());
        int damageTaken = attacker.attack(defender);
       	System.out.println(defender.getName()+" takes "+damageTaken+" damage and is left with "+defender.getCurrentHP()+"/"+defender.getMaxHP()+" HP");

        if(!defender.isAlive()) {
        	System.out.println(attacker.getName()+" defeats "+defender.getName());
        	return attacker;
        }
        else
        	return fight(defender,attacker);
	}
}
