package no.uib.inf101.v23.lecture13mutability;

public final class MyImmutable {
  private final int[] myvals;

  MyImmutable(int a, int b) {
    this.myvals = new int[] { a, b };
  }

  public int getA() {
    return this.myvals[0];
  }

  public int getB() {
    return this.myvals[1];
  }
}
