package no.uib.inf101.v23.lecture14gui.counter;

public interface ViewableModel {

  /** Get the current value of the model. */
  int getCount();
}
