package no.uib.inf101.v23.lecture14gui.counter;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;

public class View extends JPanel {
  private final ViewableModel model;

  public View(ViewableModel model) {
    this.model = model;
    this.setPreferredSize(new Dimension(150, 50));
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    g.drawString("" + this.model.getCount(), this.getWidth()/2, this.getHeight()/2);
  }
}
