package no.uib.inf101.v23.lecture14gui.counter;

public class Model implements ViewableModel {
  private int count = 0;

  @Override
  public int getCount() {
    return this.count;
  }

  /** Increment the model's value. */
  public void increment() {
    this.count++;
  }
}
