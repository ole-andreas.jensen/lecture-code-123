package no.uib.inf101.v23.lecture10;

import no.uib.inf101.v23.lecture7.IPokemon;
import no.uib.inf101.v23.lecture9inheritance.animals.Mammal;

public class Pair<T extends Mammal, V extends IPokemon> {
    
    T item1;
    V item2;

    public void placeItem1(T item1) {
        this.item1 = item1;
    }

    public void placeItem2(V item2) {
        this.item2 = item2;
    }
}
