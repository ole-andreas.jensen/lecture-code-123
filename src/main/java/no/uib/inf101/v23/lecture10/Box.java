package no.uib.inf101.v23.lecture10;

import no.uib.inf101.v23.lecture7.IPokemon;
import no.uib.inf101.v23.lecture9inheritance.animals.Feline;
import no.uib.inf101.v23.lecture9inheritance.animals.Mammal;

public class Box<T extends Feline> {
    
    T item;

    public Box() {
        
    }

    public void placeItem(T item) {
        this.item = item;
    } 

    public T removeItem() {
        T temp = this.item;
        this.item = null;
        return temp;
    }

    public T peak() {
        return this.item;
    }

}
