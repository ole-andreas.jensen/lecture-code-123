package no.uib.inf101.v23.lecture9graphics;

import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    MyFirstCanvas canvas = new MyFirstCanvas();
    JFrame frame = new JFrame();
    frame.setTitle("INF101");
    frame.setContentPane(canvas);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }
}
