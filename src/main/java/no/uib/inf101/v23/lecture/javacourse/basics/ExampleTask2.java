package no.uib.inf101.v23.lecture.javacourse.basics;

/**
 * Task: Create a new method that checks if a number is divisible by 7.
 * Return true if it is divisible, false if not.
 */
public class ExampleTask2 {

    public static void main(String[] args) {
        System.out.println(divisibleBySeven(49));
    }

    public static boolean divisibleBySeven(int num) {
        return num % 7 == 0;
    }
    
}
