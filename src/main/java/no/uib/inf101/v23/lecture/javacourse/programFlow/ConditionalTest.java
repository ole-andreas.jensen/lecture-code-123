package no.uib.inf101.v23.lecture.javacourse.programFlow;

public class ConditionalTest {
    
    public static void main(String[] args) {
        int num = 450;

        if (num < 500) {
            System.out.println("A");
        }

        if (num > 250) {
            System.out.println("B");
        }

        if (num % 7 == 0) {
            System.out.println("C");
        }
        else if (num <= 450) {
            System.out.println("D");
        }
    }

}
