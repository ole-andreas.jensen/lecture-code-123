package no.uib.inf101.v23.lecture11;

import java.util.Iterator;

public class Range implements Iterable<Integer>{

	int start;
	int end;
	
	Range(int start, int end){
		this.start = start;
		this.end = end;
	}
	
	@Override
	public Iterator<Integer> iterator() {
		return new RangeIterator(start, end);
	}

	public static Iterable<Integer> range(int start, int end) {
		return new Range(start,end);
	}
}
