package no.uib.inf101.v23.lecture11;

public class UseExceptions {

	public static void main(String[] args) {
		boolean isNum = isNumber("5");
		System.out.println(isNum);
		System.out.println(isNumber("Hei"));
		System.out.println(isNumber(null));
		System.out.println(isNumber("5.5"));
	}

	private static boolean isNumber(String string) {
		if(string==null)
			return false;
		try {
			int num= Integer.parseInt(string);
			return true;
		} catch (NumberFormatException e) {
		}
		try {
			double num= Double.parseDouble(string);
			return true;
		} catch (NumberFormatException e) {
		}
		return false;
	}

	
}
