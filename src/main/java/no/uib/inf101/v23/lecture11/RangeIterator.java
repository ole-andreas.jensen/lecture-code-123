package no.uib.inf101.v23.lecture11;

import java.util.Iterator;

public class RangeIterator implements Iterator<Integer> {

	int current;
	int end;
	
	public RangeIterator(int start, int end) {
		this.current = start;
		this.end = end;
	}

	@Override
	public boolean hasNext() {
		return current <= end;
	}

	@Override
	public Integer next() {
		int temp =current;
		current = current+1;
		return temp;
	}
}
