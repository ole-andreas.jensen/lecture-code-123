package no.uib.inf101.v23.lecture11;

public class UseArrays {

	public static void main(String[] args) {
		int[] tabell = new int[10];
		for(int i=0; i<tabell.length;i++) {
			tabell[i] = i*i;
		}

		//for(int i=1; i<=tabell.length;i++) {//feil
		for(int i=0; i<tabell.length;i++) {//rett
			System.out.println(tabell[i]);
		}

		for(int tall : tabell) {
			System.out.println(tall);
		}
		
		tabell[10] = 10*10;

	}

}
